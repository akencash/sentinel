#!/bin/bash
set -evx

mkdir ~/.akencashcore

# safety check
if [ ! -f ~/.akencashcore/.akencash.conf ]; then
  cp share/akencash.conf.example ~/.akencashcore/akencash.conf
fi
